package com.example.throughtheholes.helpers;

import android.graphics.Canvas;
import android.view.SurfaceHolder;

public class PaintScreenThread extends Thread {
    private final SurfaceHolder holder;
    private final PaintSurfaceView pSurfaceView;
    private boolean run = false;

    PaintScreenThread(SurfaceHolder holder, PaintSurfaceView pSurfaceView) {
        this.holder = holder;
        this.pSurfaceView = pSurfaceView;
    }

    public void setRun(boolean run) {
        this.run = run;
    }

    @Override
    public void run() {
        while (run) {
            Canvas c = null;
            try {
                c = holder.lockCanvas(null);
                synchronized (holder) {
                    pSurfaceView.draw(c);
                }
            } finally {
                if (c != null) {
                    holder.unlockCanvasAndPost(c);
                }
            }
        }
    }
}
