package com.example.throughtheholes.helpers;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import androidx.annotation.NonNull;

public class PaintSurfaceView extends SurfaceView implements SurfaceHolder.Callback {

    private PaintScreenThread paintScreenThread;
    private Paint paint;
    public PaintSurfaceView(Context context) {
        super(context);
        init();
    }
    private void init() {
        getHolder().addCallback(this);
        setFocusable(true);
        paint = new Paint();
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(3);
        paint.setColor(Color.WHITE);
    }

    @Override
    public void onDraw(Canvas c){

        c.drawCircle(0, 0, 50, paint);
    }

    @Override
    public void surfaceCreated(@NonNull SurfaceHolder holder) {
        paintScreenThread = new PaintScreenThread(getHolder(), this);
        paintScreenThread.setRun(true);
        paintScreenThread.start();
    }

    @Override
    public void surfaceChanged(@NonNull SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(@NonNull SurfaceHolder holder) {
        boolean retry = true;
        paintScreenThread.setRun(false);
        while (retry) {
            try {
                paintScreenThread.join();
                retry = false;
            } catch (InterruptedException e){}
        }
    }
}
