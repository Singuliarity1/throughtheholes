package com.example.throughtheholes;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.Window;
import android.widget.Button;

import androidx.annotation.NonNull;

import com.example.throughtheholes.helpers.PaintSurfaceView;

public class GameActivity extends Activity {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(new PaintSurfaceView(this));

    }
}
